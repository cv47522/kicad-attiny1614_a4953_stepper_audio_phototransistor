
#include <avr/io.h>

#define A_PLUS PIN0_bm //PB0, Black
#define A_MINUS PIN3_bm //PA3, Green
#define B_PLUS PIN2_bm //PB2, Red
#define B_MINUS PIN1_bm //PB1, Blue
#define NUM_STEPS 200
#define DELAY_STEP_US 2500
#define DELAY_CYCLE_MS 1000

#define LED_AVR PIN4_bm //PA4

void setup()
{
  PORTA.DIR = LED_AVR | A_MINUS;
  PORTB.DIR = A_PLUS | B_PLUS | B_MINUS;  // set these pins to outputs
}

void loop()
{
  int counter = 0;
  
//  while (counter < NUM_STEPS / 4) {
//    half_step_ccw();
//    counter ++;
//  }
//  counter = 0;
  
  while (counter < NUM_STEPS / 4) {
    quarter_step_ccw();
    counter ++;
  }
  PORTA.OUT |= LED_AVR;
  delay(DELAY_CYCLE_MS);
  PORTA.OUT &= ~LED_AVR;
}
/**********************************************/
// 1.8 degree/step, 200 steps/rev => per full_step_cw() has 4 steps(pulses)
void full_step_cw() {
  Ap();
  Bp();
  Am();
  Bm();
}

void full_step_ccw() {
  Bm();
  Am();
  Bp();
  Ap();
}
// 0.9 degree/step, 400 steps/rev => per half_step_ccw() has 8 steps(pulses)
void half_step_ccw() {
  Bm();
  Am_Bm();
  Am();
  Am_Bp();
  Bp();
  Ap_Bp();
  Ap();
  Ap_Bm();
}
// 0.45 degree/step, 800 steps/rev => per quarter_step_ccw() has 16 steps(pulses)
void quarter_step_ccw() {
  Bm();
  Am_Bm();
  Am_Bp_Bm();
  Am();
  Am_Bp();
  Ap_Am_Bp();
  Bp();
  Ap_Bp();
  Ap_Bp_Bm();
  Ap();
  Ap_Bm();
  Ap_Am_Bm();
}
/***************** Set this Lead to HIGH *****************************/
void Ap() {
  allGND();
  PORTB.OUT |= A_PLUS; 
  wait();
}

void Am() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  wait();
}

void Bp() {
  allGND();
  PORTB.OUT |= B_PLUS; 
  wait();
}

void Bm() {
  allGND();
  PORTB.OUT |= B_MINUS;
  wait();
}
/********************* Set these Leads to HIGH *************************/
void Ap_Bp() {
  allGND();
  PORTB.OUT |= A_PLUS | B_PLUS; 
  wait();
}
void Ap_Bm() {
  allGND();
  PORTB.OUT |= A_PLUS | B_MINUS; 
  wait();
}
void Am_Bm() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  PORTB.OUT |= B_MINUS; 
  wait();
}
void Am_Bp() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  PORTB.OUT |= B_PLUS; 
  wait();
}
/*********************/
void Ap_Am_Bp() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  PORTB.OUT |= A_PLUS | B_PLUS; 
  wait();
}
void Ap_Bp_Bm() {
  allGND();
  PORTB.OUT |= A_PLUS | B_PLUS | B_MINUS; 
  wait();
}
void Ap_Am_Bm() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  PORTB.OUT |= A_PLUS | B_MINUS; 
  wait();
}
void Am_Bp_Bm() {
  allGND();
  PORTA.OUT |= A_MINUS; 
  PORTB.OUT |= B_PLUS | B_MINUS; 
  wait();
}
/**********************************************/
void allGND() {
  PORTA.OUT &= ~A_MINUS;
  PORTB.OUT &= ~A_PLUS & ~B_PLUS & ~B_MINUS;
}

void wait() {
  delayMicroseconds(DELAY_STEP_US);
}
